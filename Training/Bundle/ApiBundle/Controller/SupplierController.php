<?php

namespace Training\Bundle\ApiBundle\Controller;

use Pim\Component\Api\Exception\PaginationParametersException;
use Pim\Component\Api\Exception\ViolationHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class SupplierController extends Controller
{

    public function getAction($code)
    {
        $supplier = $this->get('training.repository.supplier')->findOneByIdentifier($code);
        if (null === $supplier) {
            throw new NotFoundHttpException(sprintf('Supplier "%s" does not exist.', $code));
        }

        $supplierApi = $this->get('pim_serializer')->normalize($supplier, 'external_api');
        return new JsonResponse($supplierApi);
    }


    public function setAction($code, Request $request)
    {
        $data = json_decode($request->getContent(),true);

        $supplier = $this->get('training.repository.supplier')->findOneByIdentifier($code);
        if (null === $supplier) {
            throw new NotFoundHttpException(sprintf('Supplier "%s" does not exist.', $code));
        }
        $supplierUpdater = $this->get('pim_custom_entity.updater.custom_entity');

        $supplierUpdater->update($supplier,$data);
        $errors = $this->get('validator')->validate($supplier);
        if(count($errors)>0) {
            throw new ViolationHttpException($errors);
        }
        $this->get('pim_custom_entity.saver.custom_entity')->save($supplier);


        return $this->getAction($code);
    }


    public function listAction(Request $request)
    {
        $parameterValidator = $this->get('pim_api.pagination.parameter_validator');
        try {
            $parameterValidator->validate($request->query->all());
        } catch (PaginationParametersException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }

        $defaultParameters = [
            'page'       => 1,
            'limit'      => 10,
            'with_count' => 'false',
        ];

        $queryParameters = array_merge($defaultParameters, $request->query->all());
        $repository = $this->get('training.repository.supplier');
        $offset = $queryParameters['limit'] * ($queryParameters['page'] - 1);
        $order = ['name' => 'ASC'];
        $categories = $repository->searchAfterOffset([], $order, $queryParameters['limit'], $offset);

        $parameters = [
            'query_parameters'    => $queryParameters,
            'list_route_name'     => 'training_supplier_list',
            'item_route_name'     => 'training_supplier_get',
        ];
        $paginator = $this->get('pim_api.pagination.offset_hal_paginator');
        $count = true === $request->query->getBoolean('with_count') ? $repository->count() : null;
        $paginatedCategories = $paginator->paginate(
            $this->get('pim_serializer')->normalize($categories, 'external_api'),
            $parameters,
            $count
        );

        return new JsonResponse($paginatedCategories);
    }
}


