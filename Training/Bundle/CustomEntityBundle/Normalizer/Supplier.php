<?php
/**
 * Created by PhpStorm.
 * User: zyr3x
 * Date: 19.10.17
 * Time: 18.08
 */

namespace Training\Bundle\CustomEntityBundle\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\scalar;

class Supplier implements NormalizerInterface
{
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'id' => $object->getId(),
            'code' => $object->getCode(),
            'name' => $object->getName(),
            'email' => $object->getEmail(),
            'enabled' => $object->getEnabled(),
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof \Training\Bundle\CustomEntityBundle\Entity\Supplier;
    }

}