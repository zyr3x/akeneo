<?php
/**
 * Created by PhpStorm.
 * User: zyr3x
 * Date: 19.10.17
 * Time: 16.03
 */

namespace Training\Bundle\CustomEntityBundle\Entity;

use Pim\Bundle\CustomEntityBundle\Entity\AbstractCustomEntity;

class Supplier extends AbstractCustomEntity
{
    private $name;

    private $email;

    private $enabled;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    public static function getLabelProperty(): string
    {
        return 'name';
    }

    public function getCustomEntityName(): string
    {
        return 'supplier';
    }


}