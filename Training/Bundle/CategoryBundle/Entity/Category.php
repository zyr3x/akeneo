<?php
/**
 * Created by PhpStorm.
 * User: zyr3x
 * Date: 19.10.17
 * Time: 13.16
 */
namespace Training\Bundle\CategoryBundle\Entity;

class Category extends \Pim\Bundle\CatalogBundle\Entity\Category {

    private $description;

    public function getDescription(){
        return $this->description;
    }

    public function setDescription($description){
        $this->description = $description;
    }
}