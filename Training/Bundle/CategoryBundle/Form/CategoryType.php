<?php
namespace Training\Bundle\CategoryBundle\Form;


use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryType extends \Pim\Bundle\EnrichBundle\Form\Type\CategoryType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('description', TextareaType::class);
    }
}