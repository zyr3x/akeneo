<?php
/**
 * Created by PhpStorm.
 * User: zyr3x
 * Date: 18.10.17
 * Time: 16.21
 */

namespace Training\Bundle\ProductBundle\Command;

use Akeneo\Component\StorageUtils\Cursor\CursorInterface;
use Pim\Component\Catalog\Model\ProductInterface;
use Pim\Component\Catalog\Model\ProductModelInterface;
use Pim\Component\Catalog\Query\Filter\Operators;
use Pim\Component\Catalog\Updater\ProductUpdater;
use Pim\Component\Catalog\Updater\ProductModelUpdater;
use Pim\Component\Connector\ArrayConverter\FlatToStandard\ProductModel;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class ProductBundleCommand extends ContainerAwareCommand
{

    private $outputTable;
    private $errors;
    private $errorCount = 0;
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('pim:training:products')
            ->setDescription('Training Product Command');
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $products = $this->getCollection();

        $this->updateProducts($products,$output);

        $output->writeln("Error count ".$this->errorCount);

        $table = $this->getTable($output,true);

        foreach ($products as $product) {
            $this->addTableRow($product);
        }

        $table->render();

    }

    private function updateProducts(CursorInterface $products,$output)
    {
        /** @var  $productUpdateer \Pim\Component\Catalog\Updater\ProductUpdater */


        foreach ($products as $i=>$product) {
            $data = [
                'values' => [
                    "name" => [[
                        "locale" => null,
                        "scope" => null,
                        "data" => 'NEW!' . $product->getValue('name'),
                    ]]
                ]
            ];

            $dataValid = [
                'values' => [
                    "name" => [[
                        "locale" => null,
                        "scope" => null,
                        "data" => 'NEW!',
                    ]]
                ]
            ];


            if($i % 2 === 0) {
                $this->updater($product,$dataValid);
                if(count($this->errors)>0) {
                    $output->writeln("Product ".$product->getId()." invalid ".($this->errors));
                }
            } else {
                $this->updater($product,$data);
                if(count($this->errors)>0) {
                    $output->writeln("Product ".$product->getId()." invalid ".($this->errors));
                }
            }
        }
    }

    private function updater($product,$data) {
        $this->errors = [];
        $productUpdater = $this->getContainer()->get('pim_catalog.updater.product');
        $productModelUpdater = $this->getContainer()->get('pim_catalog.updater.product_model');
        $productSaver = $this->getContainer()->get('pim_catalog.saver.product');
        $productModelSaver = $this->getContainer()->get('pim_catalog.saver.product_model');
        $productValidator= $this->getContainer()->get('pim_catalog.validator.product');
        $productModelValidator = $this->getContainer()->get('pim_catalog.validator.product_model');

        if($product instanceof ProductInterface) {
            $productUpdater->update($product,$data);
            $this->errorCount++;
            $this->errors =$productValidator->validate($product);
            if(count($this->errors) == 0)
                $productSaver->save($product);
        }

        if($product instanceof ProductModelInterface) {
            $productModelUpdater->update($product,$data);
            $this->errorCount++;
            $this->errors =$productModelValidator->validate($product);
            if(count($this->errors) == 0)
                $productModelSaver->save($product);
        }

    }

    private function getTable($output,$isNew =false)
    {
        if ($this->outputTable && $isNew === false)
            return $this->outputTable;

        $this->outputTable = new Table($output);
        $this->outputTable->setHeaders(
            ['sku', 'name', /*'price', 'family', 'description econ', 'description mobile'*/]
        );
        return $this->outputTable;
    }

    private function addTableRow($product)
    {
        $id = $product instanceof ProductInterface ?
            $product->getIdentifier() :
            $product->getCode();

        $this->outputTable->addRow([
            $id,
            $product->getValue('name'),
            //$product->getValue('price')->getPrice('EUR'),
            //$product->getFamily()->getLabel(),
            //$product->getValue('description', 'en_US', 'ecommerce'),
            //$product->getValue('description', 'en_US', 'mobile'),
        ]);
    }

    private function getCollection()
    {
        /** @var @var $collectionFactory \Pim\Bundle\EnrichBundle\Elasticsearch\ProductAndProductModelQueryBuilderFactory */
        $collectionFactory = $this->getContainer()->get('pim_enrich.query.product_and_product_model_query_builder_factory');
        return $collectionFactory
            ->create(['default_locale' => "en_US", 'default_scope' => 'ecommerce'])
           // ->addFilter('categories', Operators::IN_CHILDREN_LIST, ['cameras'])
           // ->addFilter('family', Operators::IN_LIST, ['camcorders'])
           // ->addFilter('name', Operators::CONTAINS, '%NEW%')
           // ->addFilter('price', Operators::GREATER_THAN, ['amount' => 150, 'currency' => 'EUR'])
               ->addSorter('name','ASC')
            ->execute();
    }
}