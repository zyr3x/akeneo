define([
    'pim/form',
    'pim/user-context',
    'trainingui/template/username'
], function (baseForm, userContent, template) {
    return baseForm.extend({
        htmlTemplate: _.template(template),
        events:{
            click:'clicked'
        },
        render: function () {
            this.$el.html(
                this.htmlTemplate(
                    {
                        username: userContent.get('username')
                    })
            )
        },
        clicked:function(event) {
            console.log(event);
        }
    });
});