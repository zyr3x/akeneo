<?php
/**
 * Created by PhpStorm.
 * User: zyr3x
 * Date: 20.10.17
 * Time: 10.49
 */
namespace Training\Bundle\ImportExportBundle\ArrayConverter\StandardToFlat;

use Pim\Component\Connector\ArrayConverter\ArrayConverterInterface;
use Pim\Component\Connector\ArrayConverter\FieldsRequirementChecker;

class Supplier implements ArrayConverterInterface {

    private $fieldChecker;

    public function __construct(FieldsRequirementChecker $fieldChecker)
    {
        $this->fieldChecker = $fieldChecker;
    }

    public function convert(array $item, array $options = []) {
        $this->fieldChecker->checkFieldsPresence($item,['code']);
        $this->fieldChecker->checkFieldsFilling($item,['code']);
        unset($item['id']);
        return $item;
    }
}